import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Youtube } from '../models/youtube.models';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class YoutubeService {
  private youtubeUrl = {
    url: 'https://www.googleapis.com/youtube/v3',
    apikey: 'AIzaSyCNjovHag7BWfyqpD53iZwRkvcEbZSuXwE',
    playlist: 'UUuaPTYj15JSkETGnEseaFFg',
    nextPageToken: ''
  };

  constructor( private http: HttpClient) {}

  getVideos() {
    const url = `${this.youtubeUrl.url}/playlistItems`;
    const params = new HttpParams()
      .set('part','snippet')
      .set('maxResults','10')
      .set('playlistId',this.youtubeUrl.playlist)
      .set('key',this.youtubeUrl.apikey);
      
    return this.http.get<Youtube>(url, { params })
        .pipe(
          map( res => {
            this.youtubeUrl.nextPageToken = res.nextPageToken;
            return res.items;
          }),
          map( items => items.map( video => video.snippet ))
        );
  }
}
